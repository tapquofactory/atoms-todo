# atoms-todo example


A simple todo example app made with [Atoms]
Atomic Development for [todomvc]

![image](http://i45.photobucket.com/albums/f80/tapquofactory/atomstodoscreen_zps0d1e0c0f.png)

Running app
--------------

Only open /www/index.html with browser.





Making changes
--------------

Install all npm dependencies from package.json and run gulp:
```
npm install
gulp init
gulp

```






[atoms]:http://atoms.tapquo.com/
[todomvc]:http://todomvc.com
